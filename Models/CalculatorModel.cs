﻿using ConsoleCalculator.Views;
using System.Text;

namespace ConsoleCalculator.Models {
	internal class CalculatorModel : BaseModel<CalculatorView> {
		public CalculatorModel() : base(new CalculatorView()) { }

		public override void Process(string input) {
			// Этот метод производит вычисления выражения, введённого пользователем и выводит результат
			Input = input;
			var items = SplitToItems();
			var postfixItems = ToPostfix(items);
			double result = CalculatePostfix(postfixItems);

			View.View($"{result}");
		}

		// Этот метод вернёт лист элементов строки, таких как операторы, операнды и скобки
		protected List<string> SplitToItems() {
			if (string.IsNullOrEmpty(Input)) {
				throw new Exception("Некорректное значение ввода");
			}
			var items = new List<string>();

			// Этот метод вытащит из строки ближайшее число, так как числа могут быть не только одноразрядные
			string GetNextNumber(string str) {
				// Например, из строки " + 123,415 * 512,03 / (20-3,1)"
				// Вернёт значение 123,415

				StringBuilder sb = new StringBuilder();

				for (int i = 0; i < str.Length; i++) {
					// Сначала пропускаем символы отличные от цифр
					if (sb.Length == 0 && !Char.IsDigit(str[i])) continue;

					// Проверяем, если символ является цифрой или точкой, разделяющей вещественное число,
					// то добавляем его в выходную строку
					if (Char.IsDigit(str[i]) || str[i] == ',') sb.Append(str[i]);

					// Иначе завершаем цикл
					else break;
				}

				return sb.ToString();
			}

			// удалим пробелы из строки
			string input = Input.Replace(" ", String.Empty);

			// Теперь пройдём по всей строке и разобьём на составляющие - операторы, операнды и скобки
			for (int i = 0; i < input.Length; i++) {
				// Если это операнд или скобка, то добавляем
				if (!Char.IsDigit(input[i])) items.Add(input[i].ToString());
				else {
					// Если это число, то берём всё число, которое может состоять как из 1, так из нескольких символов, например 123
					string number = GetNextNumber(input.Substring(i));
					items.Add(number);
					// Если же длина полученной строки больше, чем 1, то следует индекс цикла увеличить на соответствующее количество
					if (number.Length > 1) i += number.Length - 1;
				}
			}

			return items;
		}


		// Этот метод преобразует список значения выражения с инфиксной нотацией в постфиксную нотацию
		// См. алгоритм получения постфиксной нотации
		protected List<string> ToPostfix(List<string> items) {
			var postfixItems = new List<string>();
			var stack = new Stack<string>();

			// Возвращает значение true, если первый символ имеет больший приоритет, чем второй, либо равный
			bool IsOperandHigherPriority(char a, char b) {
				var priorityTable = new Dictionary<char, int> {
					{'(', 0 }, {')', 0 },
					{'+', 1 }, {'-', 1 },
					{'*', 2 }, {'/', 2 }
				};

				return priorityTable[a] >= priorityTable[b];
			}

			// Проходимся по всем элементам списка и выстраиваем прироритет
			items.ForEach(operand => {
				// Если это число, то толкаем его в выходной список
				if (Char.IsDigit(operand[0])) postfixItems.Add(operand);

				// Если это операнд, а не число, и не закрывающая скобка
				else if (operand != ")") {
					// Если стак пустой или операнд - это открывающая скобка
					if (stack.Count == 0 || operand == "(") {
						// То просто складываем операнд в стак
						stack.Push(operand);
					}

					// Если стак не пустой
					// То выталкиваем из стака все операнды с б`ольшим приоритетом, а его складываем в стак
					else if (stack.Count > 0) {
						/*
						 * Менее удачная итерация стака
						 * 
						 * 
						// берём итератор (инумератор)
						var enumerator = stack.GetEnumerator();
						// Запоминаем сколько нужно будет удалить из стака элементов
						int numberToRemove = 0;

						// Проходимся по стаку
						while (enumerator.MoveNext()) {
							string stackedOperand = enumerator.Current;
							// И сравниваем операнды по приоритету
							if (IsOperandHigherPriority(stackedOperand[0], operand[0])) {
								// если встретился операнд с б`ольшим или равным приоритетом,
								// то выталкиваем его в список из стака
								postfixItems.Add(stackedOperand);
								// Запоминаем сколько элементов из стака нужно удалить
								numberToRemove++;
							}
							else {
								break;
							}
						}
						//Удаляем элементы из стака
						for (int i=0; i<numberToRemove; i++) {
							stack.Pop();
						}

						*/

						// Берём верхний элемент стака
						string peekOperand = stack.Peek();
						while (IsOperandHigherPriority(peekOperand[0], operand[0])) {
							// перекидываем все операнды с б`ольшим или равным приоритетом
							stack.Pop();
							postfixItems.Add(peekOperand);
							if (stack.Count > 0) peekOperand = stack.Peek(); // берём следующий элемент стака
							else break;
						}
						stack.Push(operand); // кладём в стак операнд текущий
					}
				}
				// Если текущий операнд - закрывающая скобка
				else {
					// То проходим по стаку и выталкиваем его в постфиксный список,
					// пока не встретим открывающую скобку, которую тоже удаляем из стака

					string peekOperand = stack.Pop();
					while (peekOperand != "(") {
						postfixItems.Add(peekOperand);
						peekOperand = stack.Pop();
					}
				}
			});

			// После того как прошлись по всем элементам,
			// то выталкиваем оставшиеся элементы стака в постфиксный список
			while (stack.Count > 0) {
				string operand = stack.Pop();
				postfixItems.Add(operand);
			}

			return postfixItems;
		}

		// Вычисляем результат выражения, записанного в постфиксной нотации
		// См. алгоритм
		protected double CalculatePostfix(List<string> postfixItems) {
			// этот метод возваращает результат арифметического действия над двумя числами
			double Calc(double a, double b, char op) {
				switch (op) {
					case '+': return a + b;
					case '-': return a - b;
					case '*': return a * b;
					case '/': return a / b;
					default: throw new Exception($"Неизвестный оператор \"{op}\"!");
				}
			}

			var numberStack = new Stack<double>();
			foreach (var operand in postfixItems) {
				if (Char.IsDigit(operand[0])) numberStack.Push(double.Parse(operand));
				else {
					// Проверяем наличие двух операторов в стаке
					if (numberStack.Count < 2) {
						throw new Exception("Неправильная форма постфиксной записи, перед оператором нет двух подряд идущих операндов!");
					}

					// Берём два числа из стака
					double b = numberStack.Pop();
					double a = numberStack.Pop();
					char op = operand[0];
					double calcResult = Calc(a, b, op);
					numberStack.Push(calcResult);
				}
			}

			// В стаке должно остаться одно число - это и есть результат вычисления
			if (numberStack.Count != 1) throw new Exception("Ошибка вычисления!");

			double result = numberStack.Pop();
			return result;
		}
	}
}
