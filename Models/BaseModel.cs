﻿using ConsoleCalculator.Views;

namespace ConsoleCalculator.Models {
	internal abstract class BaseModel<V> where V : BaseView {
		protected string? Input { get; set; }
		protected string? Output { get; set; }

		protected V View { get; set; }

		protected BaseModel(V view) {
			View = view;
		}

		public abstract void Process(string input);
		public virtual void Display() {
			// Выводит строку, если она инициализирована

			if (string.IsNullOrEmpty(Output)) {
				Console.WriteLine("Output error");
			} else {
				View.View(Output);
			}
		}
	}
}
