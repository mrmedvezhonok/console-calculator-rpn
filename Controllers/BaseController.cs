﻿using ConsoleCalculator.Models;
using ConsoleCalculator.Views;

namespace ConsoleCalculator.Controllers {
	internal abstract class BaseController<M, V>
		where M : BaseModel<V>
		where V : BaseView {
		protected M Model { get; set; }

		protected BaseController(M model) {
			Model = model;
		}

		public abstract void Input();

		// Метод валидации возвращает значение верности и сообщение валидации
		public virtual (bool isValid, string? message) Validate(string input) {
			bool isValid = true;
			string? message = null;

			// Проверяем если строка пуста или равна null
			if (string.IsNullOrEmpty(input)) {
				isValid = false;
				message = "Строка пуста или не инициализирована";
			}

			return (isValid, message);
		}
	}
}
