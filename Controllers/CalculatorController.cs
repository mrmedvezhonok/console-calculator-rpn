﻿using ConsoleCalculator.Models;
using ConsoleCalculator.Views;
using System.Text.RegularExpressions;

namespace ConsoleCalculator.Controllers {
	internal class CalculatorController : BaseController<CalculatorModel, CalculatorView> {
		public CalculatorController() : base(new CalculatorModel()) { }

		public override void Input() {
			string? input;
			while (true) {
				try {
					Console.WriteLine("---___---___---___---___---___---___---");
					Console.WriteLine("Введите ^ для завершения.");
					Console.WriteLine("Или введите выражение для вычисления: ");
					input = Console.ReadLine();

					if (input != null && input.Trim() == "^") {
						Console.WriteLine("До свидания!");
						break;
					}

					// Валидируем
					(bool isValid, string? message) validationResult = Validate(input);

					// Проверяем результат валидации и выводим сообщение об ошибке
					if (!validationResult.isValid) {
						Console.WriteLine(validationResult.message);
						continue;
					}
					//// либо прерываем цикл, если введена корректная строка
					//else break;

					// Вычисляем результат
					Model.Process(input!);
				}
				catch (Exception e) {
					Console.WriteLine(e.Message);
				}
			}
		}

		public override (bool, string?) Validate(string? input) {
			bool isValid;
			string? message = null;

			// Если строка не инициализирована или пуста, то она не проходит валидацию
			if (string.IsNullOrEmpty(input)) {
				isValid = false;
				message = "строка пуста или не инициализирована";

			} else {
				// Строка может содержать только цифры, арифметические операции +-*/, а также круглые скобки ()
				Regex regex = new Regex(@"^\(?[^a-zA-Z]\d[\d+\-*/\(\)]*[^a-zA-Z]$");
				isValid = regex.IsMatch(input);
				if (!isValid) {
					message = "Строка может содержать только цифры, арифметические операции +-*/, а также круглые скобки ()";
				}
				// Проверим, на совпадение количества открывающих и закрывающих скобок,
				// А также, чтобы сначала шли открывающие, а потом закрывающие
				if (input.Contains(')')) {
					// пройдёмся по строке, проверяя парны ли скобки
					/*
					 * Вариант проверки парности скобок подходит для решения этой задачи
					 * в том случае, если у нас множество видов скобок
					 * 
					var openBreakesStack = new Stack<char>();
					foreach (var c in input) {
						if(c == '(') {
							// открывающую скобку добавляем в стак
							openBreakesStack.Push(c);
						}
						// если нашли закрывающую скобку
						else if (c == ')') {
							// если стак пустой, значит, скобки не парные и расставлены некорректно
							if (openBreakesStack.Count == 0) {
								isValid = false;
								message = "Выражение некорректно, так как неправильно расставлены скобки";
								break;
							} else openBreakesStack.Pop();
						}
					}
					*/

					// Ведём подсчёт количества открытых скобок
					int openedBrakesCount = 0;
					foreach (var c in input) {
						// Учитываем открывающую скобку
						if (c == '(') openedBrakesCount++;
						
						// если нашли закрывающую скобку
						else if (c == ')') {
							// если количество незакрытых скобок = 0, значит, скобки непарные и расставлены некорректно
							if (openedBrakesCount == 0) {
								isValid = false;
								message = "Выражение некорректно, так как неправильно расставлены скобки";
								break;
							}
							// иначе учитываем закрытую скобку
							openedBrakesCount--;
						}
					}
					// Если внутри выражения не все скобки закрыты
					if (openedBrakesCount > 0) {
						isValid = false;
						message = "Выражение некорректно, так как неправильно расставлены скобки";
					}
				}
			}

			// Проверяем регулярное выражение, если оно не прошло, то валидация не пройдена
			return (isValid, message);
		}
	}
}
