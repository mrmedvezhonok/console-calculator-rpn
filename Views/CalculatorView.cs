﻿namespace ConsoleCalculator.Views {
	internal class CalculatorView : BaseView {
		public CalculatorView() { }

		public override void View(string Output) {
			Console.WriteLine($"Результат выражения равняется {Output}");
		}
	}
}
