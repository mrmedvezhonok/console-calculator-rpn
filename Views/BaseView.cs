﻿using ConsoleCalculator.Models;

namespace ConsoleCalculator.Views {
	internal abstract class BaseView {
		protected BaseView() { }

		public virtual void View(string Output) {
			Console.WriteLine(Output);
		}
	}
}
